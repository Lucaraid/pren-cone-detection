# pren-cone-detection

Traffic cone detection for the HSLU Modules PREN1 and PREN2 in 2019/2020.

Inspired by: PatrickUtz - [traffic-cone-OpenCV-detection](https://github.com/PatrickUtz/traffic-cone-OpenCV-detection)