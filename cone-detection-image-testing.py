import numpy as np
import cv2
import random as rng
import datetime

#cap = cv2.VideoCapture(0)

# check if camera opened successfully
#if (cap.isOpened()== False): 
  #print("Error opening video stream or file")

start_time = datetime.datetime.now()
total_images = 0

# read until video is completed
while(True):#cap.isOpened()):
    # capture frame-by-frame
    #ret, frame = cap.read()
    frame = cv2.imread('/home/pi/Bilder/480x640/auto4.jpg')
    ret = True
    if ret == True:
        # convert the image to HSV because easier to represent color in
        # HSV as opposed to in BGR 
        hsv_img = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

        # define range of orange traffic cone color in HSV
        lower_orange_hue = 0
        lower_orange_sat = round(55.0/100*255)
        lower_orange_val = round(70.0/100*255)
        
        higher_orange_hue = 40/2
        higher_orange_sat = round(100/100*255)
        higher_orange_val = round(100/100*255)
        
        lower_red_hue = 335/2
        lower_red_sat = round(55.0/100*255)
        lower_red_val = round(70.0/100*255)
        
        higher_red_hue = 360/2
        higher_red_sat = round(100/100*255)
        higher_red_val = round(100/100*255)
        
        lower_orange = np.array([lower_orange_hue, lower_orange_sat, lower_orange_val])
        higher_orange = np.array([higher_orange_hue, higher_orange_sat, higher_orange_val])
        
        lower_red = np.array([lower_red_hue, lower_red_sat, lower_red_val])
        higher_red = np.array([higher_red_hue, higher_red_sat, higher_red_val])

        # threshold the HSV image to get only bright orange colors
        threshed_img_orange = cv2.inRange(hsv_img, lower_orange, higher_orange)
        threshed_img_red = cv2.inRange(hsv_img, lower_red, higher_red)
        
        threshed_img = cv2.bitwise_or(threshed_img_orange, threshed_img_red)

        # smooth the image with erosion, dialation, and smooth gaussian
        # first create a kernel with standard size of 3x3 pixels
        kernel = np.ones((3,3),np.uint8)

        # get rid of small artifacts by eroding first and then dialating 
        threshed_img_smooth = cv2.erode(threshed_img, kernel, iterations = 3)
        threshed_img_smooth = cv2.dilate(threshed_img_smooth, kernel, iterations = 3)

        #cv2.imshow("threshold artifacts", threshed_img_smooth)
        
        
        #kernel = np.array([[0,0,1,0,0], [0,1,1,1,0], [0,1,1,1,0], [1,1,1,1,1], [1,1,1,1,1]],np.uint8)
        kernel = np.ones((5,3), np.uint8)
        # account for cones with reflective tape by dialating first to bridge the gap between one orange edge
        # and another and then erode to bring the traffic cone back to standard size
        smoothed_img = cv2.dilate(threshed_img_smooth, kernel, iterations = 15)
        smoothed_img = cv2.erode(smoothed_img, kernel, iterations = 15)

        cv2.imshow("close", smoothed_img)

        # detect all edges witin the image
        edges_img = cv2.Canny(smoothed_img, 100, 200)
        contours, hierarchy = cv2.findContours(edges_img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        # set parameters for writing text and drawing lines
        font = cv2.FONT_HERSHEY_SIMPLEX
        fontScale = 2
        fontColor = (0, 0, 255)
        lineType = 2

        cv2.drawContours(frame, contours, -1, (255, 255, 255), 2)

        # analyze each contour and deterime if it is a triangle
        for cnt in contours:
            boundingRect = cv2.boundingRect(cnt)
            approx = cv2.approxPolyDP(cnt, 0.1 * cv2.arcLength(cnt, True), True)
            cv2.drawContours(frame, approx, -1, (255, 0, 0), 4)
            # if the contour is a triangle, draw a bounding box around it and tag a traffic_cone label to it
            if len(approx) == 3:
                x, y, w, h = cv2.boundingRect(approx)
                rect = (x, y, w, h)
                cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 3)
                bottomLeftCornerOfText = (x, y)
                cv2.putText(frame,'traffic_cone', 
                    bottomLeftCornerOfText, 
                    font, 
                    fontScale,
                    fontColor,
                    lineType)

        # display the resulting frame
        cv2.imshow('Frame',frame)
    
        # press Q on keyboard to  exit
        if cv2.waitKey(25) & 0xFF == ord('q'):
            break
    
        total_images += 1
        time_delta = datetime.datetime.now() - start_time
        fps = total_images / time_delta.total_seconds()
        print("fps: " + str(fps))

    # break the loop
    else: 
        break
 
cv2.waitKey()
 
# when everything done, release the video capture object
cap.release()
 
# closes all the frames
cv2.destroyAllWindows()
