import numpy as np
import cv2
import datetime
import argparse

VERSION = "0.1.1"

# Field of view of the raspberry pi v2 camera module in degrees.
CAMERA_ANGLE = 62.2
# Video resolution
FRAME_WIDTH = 480
FRAME_HEIGHT = 360

# How many seconds does a lying cone to be recognized to count as a round passed.
LAYING_CONE_SECONDS_FOR_ROUND_COUNT = 2
# After how many seconds the counter will be set back
LAYING_CONE_SPLIT_SECONDS = 10
# How many rounds around the parcour should the device drive.
ROUNDS_TO_DRIVE = 2


def parse_arguments():
    # Create argument parser
    parser = argparse.ArgumentParser()

    # Optional arguments
    parser.add_argument("-d", "--display", help="displays a live-view of the frames",
                        dest="display", action="store_true")
    parser.add_argument("-o", "--output", help="saves the calculated frames to a video file",
                        dest="output", action="store_true")

    # Print version
    parser.add_argument("--version", action="version", version='%(prog)s - Version {}'.format(VERSION))

    # Parse arguments
    args = parser.parse_args()

    return args


def main(arguments):
    # output file for vehicle control
    vehicle_control_log = open("vehicle-control.log", "w")
    vehicle_control_log.write("fast\n")

    # input file for distance sensor values
    distance_sensor_log = open("distance-sensor.log", "r")

    # used for fps calculation
    start_time = datetime.datetime.now()
    total_images = 0
    fps = 0  # frames per second

    lying_cones_count = 0  # how many lying cones were detected
    rounds = 0  # rounds the device drove
    round_counted_up = False

    no_cones_start_time = None
    first_time_lying_cone_detected = None
    first_time_no_lying_cone_detected = None

    # Get a videostream from the default capturing device with resolution 480x360.
    cap = cv2.VideoCapture(0)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, FRAME_WIDTH)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT)
    out = None

    if arguments.output:
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        out = cv2.VideoWriter("pylonpi_{}.avi".format(str(start_time)), fourcc, 20.0, (FRAME_WIDTH, FRAME_HEIGHT))

    try:
        # Check if camera opened successfully.
        if not cap.isOpened():
            print("Error opening video stream.")

        # Read as long camera is capturing.
        while cap.isOpened():
            # capture frame-by-frame
            ret, frame = cap.read()

            if not ret:
                print("Error capturing frame")
                break

            # convert the image to HSV because easier to represent color in
            # HSV as opposed to in BGR/RGB
            hsv_img = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

            # Define range of orange traffic cone color in HSV.
            lower_orange_hue = 0
            lower_orange_sat = round(50.0 / 100 * 255)
            lower_orange_val = round(50.0 / 100 * 255)

            higher_orange_hue = 40 / 2
            higher_orange_sat = round(100 / 100 * 255)
            higher_orange_val = round(100 / 100 * 255)

            lower_red_hue = 335 / 2
            lower_red_sat = round(50.0 / 100 * 255)
            lower_red_val = round(50.0 / 100 * 255)

            higher_red_hue = 360 / 2
            higher_red_sat = round(100 / 100 * 255)
            higher_red_val = round(100 / 100 * 255)

            lower_orange = np.array([lower_orange_hue, lower_orange_sat, lower_orange_val], dtype=np.int32)
            higher_orange = np.array([higher_orange_hue, higher_orange_sat, higher_orange_val], dtype=np.int32)

            lower_red = np.array([lower_red_hue, lower_red_sat, lower_red_val], dtype=np.int32)
            higher_red = np.array([higher_red_hue, higher_red_sat, higher_red_val], dtype=np.int32)

            # Threshold the HSV image.
            threshed_img_orange = cv2.inRange(hsv_img, lower_orange, higher_orange)
            threshed_img_red = cv2.inRange(hsv_img, lower_red, higher_red)

            # Make one threshold image out of the two for the two ranges.
            threshed_img = cv2.bitwise_or(threshed_img_orange, threshed_img_red)

            # cv2.imshow("threshold", threshed_img)

            # Smooth the image with erosion and dilation.
            # First create a kernel with standard size of 3x3 pixels.
            kernel = np.ones((3, 3), np.uint8)

            # Get rid of small artifacts by eroding first and then dilating.
            threshed_img_smooth = cv2.erode(threshed_img, kernel, iterations=2)
            threshed_img_smooth = cv2.dilate(threshed_img_smooth, kernel, iterations=2)

            # cv2.imshow("threshold remove artifacts", threshed_img_smooth)

            # Account for white bars on the cones by dialating first to bridge the gap between one orange edge
            # and another and then erode to bring the traffic cone back to standard size.
            # triangle kernel (very slow)
            # kernel = np.array([[0,0,1,0,0], [0,1,1,1,0], [0,1,1,1,0], [1,1,1,1,1], [1,1,1,1,1]],np.uint8)
            kernel = np.ones((5, 5), np.uint8)
            closed_cones_img = cv2.dilate(threshed_img_smooth, kernel, iterations=12)
            closed_cones_img = cv2.erode(closed_cones_img, kernel, iterations=12)

            # cv2.imshow("close", closed_cones_img)
            # Repeat closing for far cones.
            '''kernel = np.ones((2, 2), np.uint8)
            closed_far_cones_img = cv2.dilate(threshed_img_smooth, kernel, iterations=5)
            closed_far_cones_img = cv2.erode(closed_far_cones_img, kernel, iterations=5)
            # Make one image out of the two closed images.
            closed_cones_img = cv2.bitwise_or(closed_cones_img, closed_far_cones_img)'''

            # cv2.imshow("far close", closed_far_cones_img)

            # Detect all edges within the image.
            edges_img = cv2.Canny(closed_cones_img, 100, 200)
            contours, hierarchy = cv2.findContours(edges_img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

            # Set parameters for writing text and drawing lines.
            font = cv2.FONT_HERSHEY_SIMPLEX
            font_scale = 0.75
            font_color = (0, 0, 255)
            line_type = 2

            cv2.drawContours(frame, contours, -1, (255, 255, 255), 2)

            pylon_angles = []
            lying_pylon_angles = []

            # analyze each contour and deterime if it is a triangle
            for cnt in contours:
                # Find outer points.
                approx = cv2.approxPolyDP(cnt, 0.07 * cv2.arcLength(cnt, True), True)
                cv2.drawContours(frame, approx, -1, (255, 0, 0), 4)
                # if the contour is a triangle, draw a bounding box around it and tag a traffic_cone label to it
                area = cv2.contourArea(cnt)
                center, size, angle = cv2.minAreaRect(cnt)

                # use longer side of rectangle for angle calculation
                if size[0] < size[1]:
                    angle = angle - 90.0

                # Use only triangles with area above 300.
                if len(approx) == 3 and area > 300:
                    x, y, w, h = cv2.boundingRect(approx)
                    cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 3)
                    x_pylon_center = x + (w / 2.0)
                    pylon_distance_from_center = 240.0 - x_pylon_center
                    pylon_angle = (pylon_distance_from_center / 240.0 * CAMERA_ANGLE / 2.0) * -1

                    # standing cone
                    if -135.0 < angle < -45.0:
                        pylon_angles.append(pylon_angle)
                        # print('pylon angle: ' + str(round(pylon_angle)) + ' deg')
                        cv2.putText(frame, 'upright cone',
                                    (x, y),
                                    font,
                                    font_scale,
                                    font_color,
                                    line_type)

                    # lying cone
                    if -45.0 < angle < 45.0 or -225.0 < angle < -135.0:
                        if first_time_lying_cone_detected is None:
                            first_time_lying_cone_detected = datetime.datetime.now()
                        lying_pylon_angles.append(pylon_angle)
                        # print('pylon angle: ' + str(round(pylon_angle)) + ' deg')
                        cv2.putText(frame, 'lying cone',
                                    (x, y),
                                    font,
                                    font_scale,
                                    font_color,
                                    line_type)

                        # if laying cone detected for specified time, next round starts
                        if LAYING_CONE_SPLIT_SECONDS >= (datetime.datetime.now() - first_time_lying_cone_detected).total_seconds() >= LAYING_CONE_SECONDS_FOR_ROUND_COUNT:
                            if round_counted_up is False:
                                rounds += 1
                                round_counted_up = True

                        if rounds >= ROUNDS_TO_DRIVE:
                            rounds = -1
                        first_time_no_lying_cone_detected = None
                    else:
                        if first_time_no_lying_cone_detected is None:
                            first_time_no_lying_cone_detected = datetime.datetime.now()

                        if (datetime.datetime.now() - first_time_no_lying_cone_detected).total_seconds() >= LAYING_CONE_SPLIT_SECONDS:
                            first_time_lying_cone_detected = None
                            first_time_no_lying_cone_detected = None
                            round_counted_up = False
                            print("more than 10 seconds no lying cones. round counting reset.")

            # cones detected and angle calculated
            if pylon_angles:
                no_cones_start_time = None
                rightest_pylon_angle = max(pylon_angles)
                steering_angle = rightest_pylon_angle + 15.0

                if lying_pylon_angles:
                    leftest_lying_pylon_angle = min(lying_pylon_angles)
                    steering_angle_temp = (rightest_pylon_angle + leftest_lying_pylon_angle) / 2.0
                    if steering_angle_temp < steering_angle:
                        steering_angle = steering_angle_temp
            # no cones detected
            else:
                if no_cones_start_time is None:
                    no_cones_start_time = datetime.datetime.now()

                # if less than 2 seconds no cones detected steer straight
                if (datetime.datetime.now() - no_cones_start_time).total_seconds() < 2:
                    steering_angle = 0
                # else steer to the left, to find cones
                else:
                    steering_angle = -15

            steering_angle = round(steering_angle)

            # write steering angle to log file
            vehicle_control_log.write(str(steering_angle) + "\n")

            if rounds == -1:
                # FINISHED, STOP DRIVING
                vehicle_control_log.write("stop\n")
                steering_angle = 0
                cv2.putText(frame, 'GAME OVER',
                            (int(FRAME_WIDTH / 6), int(FRAME_HEIGHT / 2)),
                            font,
                            font_scale * 3,
                            font_color,
                            line_type)

            # print('steering angle : ' + str(steering_angle) + ' deg')
            # Only used to indicate steering angle with a line on the frame.
            steering_x = int(round(steering_angle / (CAMERA_ANGLE / 2.0) * 240.0 + 240))
            cv2.line(frame, (steering_x, 0), (steering_x, 360), (255, 0, 0), 3)

            # read distance sensor value
            distance_value = distance_sensor_log.readline()

            if distance_value and float(distance_value) < 0.5:
                # obstacle in the way
                vehicle_control_log.write("slow\n")
            else:
                # no obstacle in the way
                vehicle_control_log.write("fast\n")

            total_images += 1

            if total_images % 10 == 0:
                time_delta = datetime.datetime.now() - start_time
                fps = total_images / time_delta.total_seconds()
                start_time = datetime.datetime.now()
                total_images = 0
                # print("fps: " + str(fps))

            # PRINT ON IMAGE
            cv2.putText(frame, 'fps: {}'.format(int(round(fps))),
                        (0, FRAME_HEIGHT - 5),
                        font,
                        font_scale,
                        font_color,
                        line_type)

            cv2.putText(frame, 'steering angle: {}'.format(int(round(steering_angle))),
                        (0, FRAME_HEIGHT - 30),
                        font,
                        font_scale,
                        font_color,
                        line_type)

            cv2.putText(frame, 'round: {}'.format(int(rounds + 1)),
                        (0, FRAME_HEIGHT - 55),
                        font,
                        font_scale,
                        font_color,
                        line_type)
            cv2.putText(frame, 'distance sensor: {}'.format(distance_value),
                        (0, FRAME_HEIGHT - 80),
                        font,
                        font_scale,
                        font_color,
                        line_type)

            # display the resulting frame
            if arguments.display:
                cv2.imshow('Frame', frame)

            # save the resulting frame to a video output
            if arguments.output:
                out.write(frame)

            # press Q on keyboard to  exit
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
    finally:
        # When everything done, release the video capture object, close all windows, close the out stream and log files.
        cap.release()
        cv2.destroyAllWindows()
        vehicle_control_log.write("stop\n")
        vehicle_control_log.close()
        distance_sensor_log.close()

        if arguments.output:
            out.release()


if __name__ == "__main__":
    main(parse_arguments())
